stsci.tools (4.2.0-1) unstable; urgency=medium

  * New upstream version 4.2.0
  * Drop Update-test_compmixin.py.patch, applied upstream

 -- Ole Streicher <olebole@debian.org>  Fri, 07 Feb 2025 08:52:50 +0100

stsci.tools (4.1.1-2) unstable; urgency=medium

  * Update test_compmixin.py (Closes: #1082319)

 -- Ole Streicher <olebole@debian.org>  Fri, 20 Sep 2024 13:27:21 +0200

stsci.tools (4.1.1-1) unstable; urgency=medium

  * New upstream version 4.1.1
  * Rediff patches
  * Push Standards-Version to 4.7.0. No changes needed
  * Build-Depend on pybuild-plugin-pyproject
  * Enforce pytest run parameters, overwriting pybuild.toml ones
    that don't work with pybuild

 -- Ole Streicher <olebole@debian.org>  Mon, 01 Jul 2024 19:37:38 +0200

stsci.tools (4.1.0-1) unstable; urgency=medium

  * New upstream version 4.1.0
  * Drop Update-exiting-copy-over-FitsTestCase.patch: applied upstream
  * Push Standards-Version to 4.6.2. No changes needed

 -- Ole Streicher <olebole@debian.org>  Wed, 27 Sep 2023 10:29:46 +0200

stsci.tools (4.0.1-3) unstable; urgency=medium

  * Update exiting copy over FitsTestCase
  * Push Standards-Version to 4.6.1, no changes needed

 -- Ole Streicher <olebole@debian.org>  Sat, 17 Dec 2022 11:42:15 +0100

stsci.tools (4.0.1-2) unstable; urgency=medium

  * Switch back to master

 -- Ole Streicher <olebole@debian.org>  Fri, 29 Oct 2021 09:51:39 +0200

stsci.tools (4.0.1-1) experimental; urgency=medium

  * New upstream version 4.0.1
  * Switch to experimental (incompatible new version)
  * Rediff patches
  * Update dependencies
  * Push Standards-Version to 4.6.0. No changes needed
  * Add "Rules-Requires-Root: no" to d/control
  * Add SETUPTOOLS_SCM_PRETEND_VERSION to d/rules
  * Push dh-compat to 13
  * Enable build time and CI tests

 -- Ole Streicher <olebole@debian.org>  Mon, 06 Sep 2021 21:02:22 +0200

stsci.tools (3.6.0-1) unstable; urgency=low

  * New upstream version 3.6.0
  * Add d/gitlab-ci.yml for salsa
  * Switch to debhelper-compat

 -- Ole Streicher <olebole@debian.org>  Fri, 02 Aug 2019 08:38:48 +0200

stsci.tools (3.5.2-1) unstable; urgency=low

  * New upstream version 3.5.2
  * Push Standards-Version to 4.4.0. No changes needed
  * Push compat to 12

 -- Ole Streicher <olebole@debian.org>  Thu, 18 Jul 2019 14:24:51 +0200

stsci.tools (3.4.13-2) unstable; urgency=low

  * Remove Python 2 package
  * Push Standards-Version to 4.2.1. No changes needed

 -- Ole Streicher <olebole@debian.org>  Sat, 15 Dec 2018 13:42:27 +0100

stsci.tools (3.4.13-1) unstable; urgency=low

  * New upstream version 3.4.13. Rediff patches
  * Push Standards-Versionto 4.2.0. No changes required
  * Push compat to 11

 -- Ole Streicher <olebole@debian.org>  Thu, 09 Aug 2018 14:21:44 +0200

stsci.tools (3.4.12-1) unstable; urgency=medium

  * Update VCS URLs to salsa.d.o
  * New upstream version 3.4.12. Rediff patches
  * Push Standards-Version to 4.1.3. No changes needed.
  * Use a fake relic package for version info

 -- Ole Streicher <olebole@debian.org>  Thu, 15 Feb 2018 08:54:36 +0100

stsci.tools (3.4.11-1) unstable; urgency=low

  * Initial release. (Closes: #884598)

 -- Ole Streicher <olebole@debian.org>  Wed, 20 Dec 2017 08:49:59 +0100
